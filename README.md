<img src="https://simpleicons.org/icons/next-dot-js.svg" width="100" height="100">

test

#  [Roadmap](https://gitlab.com/elzinko/nextjs/-/boards)

# [Changelog](./CHANGELOG.md)

# Development

## Install

```shell
$ npm install
```

## Develop

```shell
$ npm dev
```

## Build for production

```shell
$ npm build
```

# Docker

First, you should install [docker](https://www.docker.com/products/docker-desktop).

## build

```shell
$ docker build -t nextjs .
```

## check built image

```shell
$ docker images
```

## run

```shell
$ docker run -d -p 3000:3000 nextjs
```

## check running container

```shell
$ docker ps
```

## registry

```shell
$ docker login registry.gitlab.com
$ docker build -t registry.gitlab.com/elzinko/nextjs .
$ docker push registry.gitlab.com/elzinko/nextjs
$ docker run -d -p 3000:3000 registry.gitlab.com/elzinko/nextjs:latest
```

# Deployment

The developpement flow should follow [Gitlab flow](https://www.youtube.com/watch?v=InKNIvky2KE&feature=youtu.be) recommandations.

Push a new commit on master then check the pipeline on [Gitlab](https://gitlab.com/elzinko/nextjs/pipelines)

The pipeline apply multiple steps :
* the image is built using docker
* the image is tagged as master
* the master image is tagged as latest
* the image is automatically deployed on Azure when a new image taged as 'latest' is detected

## Now

To install now :
```
$ npm install -g now
```

The source code is automatically deployed on Now. Check the 'now' step in [pipeline](https://gitlab.com/elzinko/nextjs/pipelines) 

You can also trigger a new deployment mannually using a single command from your terminal with [Now CLI](https://zeit.co/download) :

```shell
$ now
```

You can also trigger new images using :

```
$ now --container
```

https://portal.azure.com/?websitesextension_ext=asd.featurePath%3Ddetectors%2FLinuxAppDown#@ecoceaoutlook.onmicrosoft.com/resource/subscriptions/aea1efc7-a587-43c9-80a7-230f0277f0f8/resourceGroups/nextjs-resources-group/providers/Microsoft.Web/sites/nextjs-vscode/appServices

### secrets

Please refer to these posts to handle secrets :
* [Legacy](https://nextjs.org/docs/api-reference/next.config.js/environment-variables)
* [Now](https://www.leighhalliday.com/secrets-env-vars-nextjs-now)

## Azure

To connect to Azure, use this informations : 
* url : [https://portal.azure.com/](https://portal.azure.com/)
* login : ecocea@outlook.com
* password : ask to thomas.couderc@gmail.com

Check all the [scripts](./devel/scripts/) to create a new Azure project from scratch.

On Azure, a [CI/CD project](https://portal.azure.com/?websitesextension_ext=asd.featurePath%3Ddetectors%2FLinuxAppDown#@ecoceaoutlook.onmicrosoft.com/resource/subscriptions/aea1efc7-a587-43c9-80a7-230f0277f0f8/resourceGroups/nextjs-resources-group/providers/Microsoft.Web/sites/nextjs-vscode/appServices) has been configured to trigger a deployment as soon as a new image is taged as latest.

# Infrastructure

* [terraform](https://www.terraform.io/docs/providers/azurerm/index.html)
* [Ansible Galaxy](https://galaxy.ansible.com/)

- Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- Manually Deploying an Application on AKS
- Automated build & deployment using Gitlab CI/CD

#### TL;TR

```
* az login
* az ad sp create-for-rbac --name <serviceName>
* in secrets.tfvars fill service_principal and service_principal_password using appId and password in previous response
* terraform init
* terraform plan --var-file=secrets.tfvar
* terraform apply --var-file=secrets.tfvars
```

#### Getting Started
- [00 - Prerequisites](docs/00_prerequisites.md)

#### Manually Containerising a 'Hello World' Application
- [02 - Containerising an Application using Docker Desktop](docs/02_containerising.md)
- [03 - Manually Pushing an Image to Gitlab Container Registry](docs/03_pushing_to_gitlab_container_registry.md)

#### Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- [04 I - Creating an AKS Cluster Using Terraform or Azure CLI](docs/04_I_creating_aks_via_terraform.md)
- [04 II - Creating an AKS Cluster Using the Azure Portal](docs/04_II_creating_aks_via_azure_portal.md)
- [05 - Connecting Gitlab to AKS](docs/06_connecting_gitlab_to_aks.md)

#### Manually Deploying an Application on AKS
- [06 - Manually Deploying a "Hello World" Application to AKS](docs/06_deploying_to_aks.md)

#### Automated build & deployment using Gitlab CI/CD
- [07 - Using Gitlab CI/CD to build an Image](docs/07_gitlab_ci_build_image.md)
- [08 - Using Gitlab CI/CD to deploy an Image](docs/08_gitlab_ci_deploy_image.md)
- [09 - Troubleshooting Advice](docs/10_troubleshooting_deployments.md)


# Links

* [environment variables](https://www.npmjs.com/package/next-env)
* [nextjs doc](https://nextjs.org/docs/api-reference/next.config.js/introduction)
* [semantic versionning](https://github.com/mrooding/gitlab-semantic-versioning)
* [multiple project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html)
* [secrets](https://www.leighhalliday.com/secrets-env-vars-nextjs-now)
* [gitlab 11 rules](https://about.gitlab.com/blog/2016/07/27/the-11-rules-of-gitlab-flow/)
* [gitlab flow overview](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/)
* [feature flag](https://docs.gitlab.com/ee/development/feature_flags/)
* [now dev](https://zeit.co/blog/now-dev)
* [react-unleash-flags](https://www.npmjs.com/package/react-unleash-flags)
