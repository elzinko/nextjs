FROM node:10-alpine AS builder

LABEL maintainer="thomas.couderc@gmail.com"

WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build


FROM node:10-alpine

ENV NODE_ENV production
ENV PORT 8080

WORKDIR /app
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/next.config.js ./next.config.js
EXPOSE $PORT
CMD ["node_modules/.bin/next", "start"]