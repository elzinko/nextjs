# CHANGELOG

* [x] documented
* [x] gitlabed
* [x] dockerized
* [x] Azurified
* [x] Feature flagged
* [x] terraformed
* [x] AKSed
* [x] Gitlab Flowed
* [x] Versionning
