import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'
import * as Sentry from '@sentry/browser'
import { FlagsProvider, FeatureFlag } from 'react-unleash-flags'

Sentry.init({dsn: "https://6b7091ddc97e481cba449375951ec7ce@sentry.io/1884519"});

const flagConfig = {
  appName: 'nextjs',
  url: 'https://gitlab.com/api/v4/feature_flags/unleash/16356346',
  instanceId: 'YSeMc1hyW3sPjfeLpUs3',
};

function Home () {

  return (
    <FlagsProvider config={flagConfig} >
      <div>
        <Head title="Home" />
        <Nav />

        <div className="hero">
          <h1 className="title">Welcome to Next!</h1>

          <FeatureFlag name="my-new-feature">
            <p className="feature">my new feature !</p>
          </FeatureFlag>

          <FeatureFlag name="feature-2">
            <p className="feature">feature 2 is here</p>
          </FeatureFlag>

          <FeatureFlag name="feature-3">
            <p className="feature">feature 3 is here</p>
          </FeatureFlag>

        </div>

        <style jsx>{`
          .hero {
            width: 100%;
            color: #333;
          }
          .title {
            margin: 0;
            width: 100%;
            padding-top: 80px;
            line-height: 1.15;
            font-size: 48px;
          }
          .title,
          .description {
            text-align: center;
          }
          .feature {
            text-align: center;
            font-size: 30px;
            color: #067df7;
          }
        `}</style>
      </div>
    </FlagsProvider>
  );
};

export default Home;
