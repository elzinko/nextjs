#!/bin/sh

source ./00-variables.sh

##################
# create project #
##################

# create a deployment user
az login -u $USERNAME -p $PASSWORD
az webapp deployment user show
az webapp deployment user set --user-name $USERNAME --password $PASSWORD

# create a resource groupe
az group list
az group create --name $RESOURCE_GROUP_NAME --location $GROUPE_LOCATION
az configure --defaults group=$RESOURCE_GROUP_NAME

# Create a Service Plan for your Resource Group
az appservice plan create --name $SERVICE_PLAN_NAME -g $RESOURCE_GROUP_NAME --sku S1 --is-linux

# configure container port
az webapp config appsettings set -g $RESOURCE_GROUP_NAME -n $WEBAPP_NAME --settings WEBSITES_PORT=3000

# set registry config
az webapp config container set -g $RESOURCE_GROUP_NAME -n $WEBAPP_NAME --docker-registry-server-url $REGISTRY_URL --docker-custom-image-name $IMAGE_TAG

# enable CD using images
CI_CD_URL=$(az webapp deployment container show-cd-url -g nextjs-resources-group -n nextjs-vscode | docker run --rm -i imega/jq '.CI_CD_URL')
az webapp deployment container show-cd-url -g $RESOURCE_GROUP_NAME -n $WEBAPP_NAME
az acr webhook create -n $WEBHOOK_NAME -r $REGISTRY_NAME --uri $CI_CD_URL --actions push

