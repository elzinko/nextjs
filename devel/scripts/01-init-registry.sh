#!/bin/sh

source ./00-variables.sh

###################
# create registry #
###################

az acr create -n $REGISTRY_NAME --resource-group $RESOURCE_GROUP_NAME --sku Basic

# create an admin account
az acr update -n $REGISTRY_NAME --admin-enabled true

# show credentials
az acr credential show -n $REGISTRY_NAME

# check registry login 
# az acr login -n manutan

# registry should be empty at first
az acr repository list -n $REGISTRY_NAME
