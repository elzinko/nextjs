Build image
===========

az acr login -n containerregistrymanutan
docker build -t containerregistrymanutan.azurecr.io/nextjs .

Run image
=========
docker run -it --rm -p 3000:3000 containerregistrymanutan.azurecr.io/nextjs

Tag image
=========
docker tag nextjs containerregistrymanutan.azurecr.io/nextjs

Push image
==========
docker push containerregistrymanutan.azurecr.io/nextjs:latest

Check image
===========
az acr repository list -n containerregistrymanutan


deploy image
============

az webapp config container set -n nextjs-vscode -g nextjs-resources-group --docker-registry-server-url https://containerregistrymanutan.azurecr.io --docker-custom-image-name containerregistrymanutan.azurecr.io/nextjs

Assign a port
=============
az webapp config appsettings set -g nextjs-resources-group -n nextjs-vscode --settings WEBSITES_PORT=3000


restart app
===========
az webapp restart --name nextjs-vscode --resource-group nextjs-resources-group


log app
=======
az webapp log tail --name nextjs-vscode --resource-group nextjs-resources-group



CD WEBHOOK
==========

https://www.nearform.com/blog/deploying-containerized-node-js-applications-with-microsoft-azure/

az webapp deployment container config --enable-cd true  -g <resource-group-name> -n <app-name>
https://$<app-name>:ciHitjfZQ3CyDmTYTraLQKLeeca1H5KwhxukHauG2Ts5yPotP0JTq4EAFJNN@<app-name>.scm.azurewebsites.net/docker/hook
az webapp deployment container show-cd-url -g <resource-group-name> -n <app-name>
az acr webhook create -n <webhook-name> -r <container-registry-name> --uri <cd-uri> --actions push


OTHERS
======



stages:
  - deploy-azure

deploy-azure:
  image: nexus.ecocea.com:8090/ecocea/node-chromium
  stage: deploy-azure
  script:
    - npm install
    - npm run build:prod
  tags:
    - docker
  environment:
    name: int
    url: https://int-wcs8-man.ecocea.com/fr/maf
  only:
    - develop



https://docs.microsoft.com/fr-fr/cli/azure/create-an-azure-service-principal-azure-cli?view=azure-cli-latest


PS D:\dev\projects\nextjs\nextjs> az ad sp create-for-rbac --name ServicePrincipalName
Changing "ServicePrincipalName" to a valid URI of "http://ServicePrincipalName", which is the required format used for service principal names
Creating a role assignment under the scope of "/subscriptions/aea1efc7-a587-43c9-80a7-230f0277f0f8"
{
  "appId": "b1b00d12-390f-45a9-83cd-390b0a377c41",
  "displayName": "ServicePrincipalName",
  "name": "http://ServicePrincipalName",
  "password": "9259bc3a-eca1-4028-951b-ebfaf219c959",
  "tenant": "b995c7a1-e96e-4f13-9fbc-827f7da07d07"
}


PS D:\dev\projects\nextjs\nextjs> az ad sp create-for-rbac --name service-principal-nextjs
Changing "service-principal-nextjs" to a valid URI of "http://service-principal-nextjs", which is the required format used for service principal names
Creating a role assignment under the scope of "/subscriptions/aea1efc7-a587-43c9-80a7-230f0277f0f8"
  Retrying role assignment creation: 1/36
{
  "appId": "ffc4fcbf-b2ba-4090-9122-f185f334fc5b",
  "displayName": "service-principal-nextjs",
  "name": "http://service-principal-nextjs",
  "password": "3db7e77a-292c-40f2-a653-9f57acde500d",
  "tenant": "b995c7a1-e96e-4f13-9fbc-827f7da07d07"
}


PS D:\dev\projects\nextjs\nextjs> az ad sp create-for-rbac
Creating a role assignment under the scope of "/subscriptions/aea1efc7-a587-43c9-80a7-230f0277f0f8"
  Retrying role assignment creation: 1/36
  Retrying role assignment creation: 2/36
{
  "appId": "98c44b0c-a9da-4e9d-a27e-b9830173fad8",
  "displayName": "azure-cli-2020-01-17-16-51-59",
  "name": "http://azure-cli-2020-01-17-16-51-59",
  "password": "e62f4b84-8c02-408f-b4e2-205e3fbafed4",
  "tenant": "b995c7a1-e96e-4f13-9fbc-827f7da07d07"
}


az role assignment list --assignee ffc4fcbf-b2ba-4090-9122-f185f334fc5b



az webapp log tail -n nextjs-vscode -g nextjs-resources-group


CONTAINER REGISTRY
==================
sample : https://www.nearform.com/blog/deploying-containerized-node-js-applications-with-microsoft-azure/

- install docker on your computer
- az acr create -n ContainerRegistryManutan -g nextjs-resources-group --sku Basic 
- az acr update -n ContainerRegistryManutan --admin-enabled true
- az acr credential show -n ContainerRegistryManutan 
- az acr repository list -n ContainerRegistryManutan (noting




Deployer un conteneur docker
=============================
az webapp create -g nextjs-resources-group --plan ASP-nextjsresourcesgroup-a194 -n nextjs-vscode --deployment-container-image-name kitematic/hello-world-nginx


LOGS
=====
az webapp log tail --name nextjs-vscode --resource-group nextjs-resources-group


Create registry
===============
/!\ remplacer containerregistrymanutan par manutan ou ecocea

az acr create --name containerregistrymanutan --resource-group nextjs-resources-group --sku Basic

create an admin account
az acr update -n containerregistrymanutan --admin-enabled true

show credentials
az acr credential show -n containerregistrymanutan
{
  "passwords": [
    {
      "name": "password",
      "value": "7NcD29hcDWUjkeBwpVg2bdV1N+ZGwbpt"
    },
    {
      "name": "password2",
      "value": "3XqZNVDeJK58OvShpBU+f9u5lKkqceOp"
    }
  ],
  "username": "ContainerRegistryManutan"
}

