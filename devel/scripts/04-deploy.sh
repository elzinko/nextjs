#!/bin/sh

source ./00-variables.sh

#########
# Build #
#########

# login using az client
az acr login -n $REGISTRY_NAME

# build
docker tag $WEBAPP_NAME $REGISTRY_BASE
docker push $REGISTRY_BASE:latest

# get infos
az acr repository list -n $REGISTRY_NAME