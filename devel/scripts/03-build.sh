#!/bin/sh

source ./00-variables.sh

#########
# Build #
#########

# login using az client
az acr login -n $REGISTRY_NAME

# push image => deploy
docker push $REGISTRY_BASE:latest

# get infos
az acr repository list -n containerregistrymanutan